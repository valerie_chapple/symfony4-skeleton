# Symfony Skeleton
> Starting configuration for Symfony 4 with docker-compose, nginx, and mysql.


## Installation

Install `docker`, `composer` and `docker-composer`.

Then in the root project directory, which has the `composer.json` file, run:

```sh
composer install
```

## Build

Then in the root project directory, which has the `docker-compose.yml` file, run the following to build the containers, start containers, and stop containers.

```sh
# Build images on first run and after changes to
# ./docker and docker-compose.yml configurations
docker-compose build  

# Start container services in background
docker-compose up -d

# Stop containers
docker-compose down
```

To see containers and stats, run:

```sh
# Running containers
docker ps

# All containers
docker ps -a
```
Example output:
```

CONTAINER ID        IMAGE       COMMAND                  CREATED             STATUS              PORTS                                                NAMES
ada55b1a0eb5        my_nginx    "docker-php-entrypoi�"   42 minutes ago      Up 42 minutes       0.0.0.0:80->80/tcp, 0.0.0.0:443->443/tcp, 9000/tcp   my_nginx_container

```

To restart a container, use the container name from the `docker ps` command, such as `my_nginx_container` in the example above, in the command below:

```sh
docker restart my_nginx_container
```

## Configuration

### .env

The `.env` file is the default file to hold environment variables specific to the machine running the Symfony code. It is to hold values that change from machine to machine, such as database name, passwords, and urls.

You can set up several `.env` files at each stage of development. All files are loaded if it exists with each one replacing the one before. Below is the order of precedence:

```
.env  
.env.local
.env.$APP_ENV
.env.$APP_ENV.local   # overrules all of the above
```

**Note:** `.local` files are not to be committed to the repo. See Symfony docs for more information.

### docker-compose.yml

The glue holding all this together is the `docker-compose.yml` file. In it, you will see 4 basic containers/services created that can be modified to suite your needs.

1. `mysql`   - database connection
2. `nginx`   - web server
3. `php`     - runs php code on the web server
4. `adminer` - database access through browser


 Take note of the ports exposed for each service.  A port of `"3307:3306"` means `3307` is the port on your machine, and `3306` is the port inside the docker container.


### ./docker/mysql

This service is configured to download mysql 8.0 and continuously restart on failure.  The volumes option copies the ./mysql folder created during `docker-compose build` to the docker container at `/var/lib/mysql`.

The environment option utilizes the environment variables stored in the most local `.env`.  You may add more variables in the `.env` files. You can reference a variable, such as `MY_ENV_VARIABLE` by inserting the variable between the brackets: `${}`, like `${MY_ENV_VARIABLE}`

### nginx

This setup utilizes a Dockerfile in the folder `./docker/nginx/` to download the image, update nginx, copy configuration files, and expose ports prior to running nginx.

There is documentation for nginx's extensive configuration capability. This skeleton utilizes the basics, as well as adds a symfony configuration (see the file `default_symfony`).

##### ./docker/nginx/nginx.conf

This file was cleaned from the [Cleverti article](https://medium.com/@mkt_43322/how-to-setup-docker-for-a-symfony-project-26d304359592). It is a basic nginx configuration file. Note that at the top it sets the user to `www-data`, which will be referenced in a later configuration.

##### ./docker/nginx/sites-enabled/default_symfony
 This file is from the [Symfony Documentation](https://symfony.com/doc/current/setup/web_server_configuration.html#nginx).  A couple minor changes were done to match the rest of the repo configuration files.

```
server {  # Changed this...
  server_name domain.tld www.domain.tld;
  root /var/www/html/symfony/public;
  ...

server { # To this...
  server_name mydomain.test;   
  root /var/www/html/symfony/public;
  ...

```

Whatever you change the `server_name` to (such as `mydomain.test`), be sure to add it to your hosts file with admin/root privileges. For OS/Linux, start by trying in `sudo vi /etc/hosts`.  On Windows you can open Notepad as an Admin and then open the hosts file. See [SiteGround docs](https://www.siteground.com/kb/how_to_use_the_hosts_file/) for help.

### php
This setup utilizes a Dockerfile in the folder `./docker/php/` to download the image, update various php cli, copy configuration files, and expose ports prior to running php-fpm7.1.

##### ./docker/php/conf/\*.conf

The files `php-fpm.conf` and `www.conf` are copied over during the build.  The `www.conf` file references the `wwww-data` user mentioned above.  This allows the services to access files within the container.

## Meta

Valerie Chapple � [@capnchapple](https://twitter.com/capnchapple) � me@valeriechapple.com

* [http://valeriechapple.com](http://valeriechapple.com)
* [https://github.com/vchapple17](https://github.com/vchapple17)
